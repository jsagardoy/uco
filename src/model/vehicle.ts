export interface VehicleEntity {
    id: number;
    brand: string;//marca
    model: string;
    type: string;
    plate: string;
    frame: string;
    pic?: string[];
}