export interface FamiliarEntity{
    id: number;
    name: string;
    familiarPics?: string[];
    familiarAddress?: string;
    adressLink?:string;
    related?: string;
}