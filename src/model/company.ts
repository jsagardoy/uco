export interface CompanyEntity {
    id: number;
    name: string;
    cif: string;
    address: string;
    map: string;
}